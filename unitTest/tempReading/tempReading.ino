#include <OneWire.h>
#include <DallasTemperature.h>


#define Pin 10 //Se declara el pin donde se conectará la DATA

OneWire ourWire(Pin); //Se establece el pin declarado como bus para la comunicación OneWire

DallasTemperature sensors(&ourWire); //Se instancia la librería DallasTemperature

void setup() {
  delay(1000);
  SerialUSB.begin(9600);
  SerialUSB.println("Inicializando sensores...");
  sensors.begin(); //Se inician los sensores
  SerialUSB.println("Sensores inicializados");
}

void loop() {
  sensors.requestTemperatures(); //Prepara el sensor para la lectura

  SerialUSB.print(sensors.getTempCByIndex(0)); //Se lee e imprime la temperatura en grados Celsius
  SerialUSB.println(" grados Centigrados");
  SerialUSB.print(sensors.getTempFByIndex(0)); //Se lee e imprime la temperatura en grados Fahrenheit
  SerialUSB.println(" grados Fahrenheit");

  delay(1000); //Se provoca un lapso de 1 segundo antes de la próxima lectura

}
