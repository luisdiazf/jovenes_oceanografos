/*
*
* 
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this code.  If not, see <http://www.gnu.org/licenses/>.
*
* The circuit:
* SD card attached to SPI bus as follows:
* MOSI - pin 11
* MISO - pin 12
* CLK - pin 13
* CS - pin 4
*/


#include <SPI.h>
#include <SD.h>

File dataFile;

int mps = 5; //muestras por segundo

char filename[] = "LOGGER00.TXT";

int temperature = 0;

void setup() {

  delay(2000);
  
  SerialUSB.begin(9600);

  while (!SerialUSB) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  SerialUSB.println("MOSI " + String(MOSI));
  SerialUSB.println("MISO " + String(MISO));
  SerialUSB.println("SCK " + String(SCK));
  SerialUSB.println("SS " + String(SS));
  
  SerialUSB.print("Initializing SD card...");
  SerialUSB.println(CS_SD);

  // Initialize the ISL29125 with simple configuration so it starts sampling
  
    
  while (!SD.begin(CS_SD)) {
    SerialUSB.println("Card failed, or not present");
    delay(3000);
  }
  SerialUSB.println("card initialized.");
  
  
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i/10 + '0';
    filename[7] = i%10 + '0';
    if (! SD.exists(filename)) {
      break;  // leave the loop!
    }
  }
}

void loop() {
  readFakeSensors();
  dataFile = SD.open(filename, FILE_WRITE);
  if (dataFile) writeToSd(dataFile);
  else dataFile.close();
  delay(1000/mps);
}

void readFakeSensors(){
  temperature = random(15,25);
  delay(1000);
  }

void writeToSd(File archivo){
   
  //VALORES DEL SENSOR
  archivo.print(temperature);
  archivo.println();
  dataFile.close();
    
}
