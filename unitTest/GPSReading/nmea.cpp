#include "Arduino.h"
#include <stdlib.h>
#include "nmea.h"

/* returns pointer to allocated memory for data structure */
nmea_data_t parse_nmea_init_data(void)
{
  nmea_data_t p = (nmea_data_t)malloc(sizeof(struct nmea_data_struct));
  return p;
}

/* converts between NMEA and decimal degrees */
void parse_nmea_convert_degrees(char* coord)
{
  float temp;
  uint16_t deg;
  float minut;
  int32_t decminut;
  
  // convert to float
  temp = atof(coord);

  deg = temp / 100.0f;
  minut = temp - (deg * 100.0f);
  minut = minut / 60.0f;
  minut = minut * 100000000;
  decminut = round(minut);

  sprintf(coord, "%d.%d", deg, decminut);
    
}

/* parses nmea GGA string filling fields of data structure */
int parse_nmea_gga(nmea_data_t data, char* packet)
{

  uint8_t i;
  uint8_t gpindex=0;

  /* start after $GPGGA, */
  i=7;
  
  if((packet[i]==',') & (packet[i+1]==',')) /* empty? */
    return 1; /* error */

  /* time */
  while(packet[i] != ',') {
    data->gpstime[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->gpstime[gpindex]='\0'; /* end of string */

  i++;
  gpindex=0;

  /* now extract time */
  char ts[3];
  ts[0] = data->gpstime[0];
  ts[1] = data->gpstime[1];
  ts[2] = 0;
  data->hour = atoi(ts);
  data->hour = data->hour + TIMEZONE_DEVIATION;
  
  ts[0] = data->gpstime[2];
  ts[1] = data->gpstime[3];
  ts[2] = 0;
  data->minute = atoi(ts);
  
  ts[0] = data->gpstime[4];
  ts[1] = data->gpstime[5];
  ts[2] = 0;
  data->second = atoi(ts);
  
  

  /* latitude */
  while(packet[i] != ',')
  {
    data->latitude[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->latitude[gpindex]='\0';

  i++;
  gpindex=0;

  /* N/S */
  while(packet[i] != ',')
  {
    data->ns[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->ns[gpindex]='\0';

  i++;
  gpindex=0;

  /* longitude */
  while(packet[i] != ',')
  {
    data->longitude[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->longitude[gpindex]='\0';

  i++;
  gpindex=0;

  /* E/W */
  while(packet[i] != ',')
  {
    data->ew[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->ew[gpindex]='\0';

  i++;
  gpindex=0;

  /* fix quality */
  while(packet[i] != ',')
  {
    data->fix_quality=packet[i];
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* number of satellites */
  while(packet[i] != ',')
  {
    data->num_sats=packet[i];
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* horizontal dilution */
  while(packet[i] != ',')
  {
    /* do nothing */
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* altitude */
  while(packet[i] != ',')
  {
    data->altitude[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->altitude[gpindex]='\0';

  i++;
  gpindex=0;

  /* reached here, nice! */

  /* Ivalid data if fix is 0 */
  if (data->fix_quality == '0')
    return 1;

  /* convert coordinates */
  parse_nmea_convert_degrees(data->latitude);
  parse_nmea_convert_degrees(data->longitude);  

  return 0;
}

/* parses nmea RMC string filling fields of data structure */
int parse_nmea_rmc(nmea_data_t data, char* packet)
{

  uint8_t i;
  uint8_t gpindex=0;

  /* start after $GPRMC, */
  i=7;
  
  if((packet[i]==',') & (packet[i+1]==',')) /* empty? */
    return 1; /* error */

  /* time, don't care*/
  while(packet[i] != ',') {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* status, don't care*/
  while(packet[i] != ',') {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* latitude, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* N/S, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* longitude, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* E/W, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* speed, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* track angle, don't care */
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* date */
  while(packet[i] != ',') {
    data->gpsdate[gpindex]=packet[i];
    i++;
    gpindex++;
  }   
  data->gpstime[gpindex]='\0'; /* end of string */

  i++;
  gpindex=0;

  /* now extract date */
  char ts[3];
  ts[0] = data->gpsdate[0];
  ts[1] = data->gpsdate[1];
  ts[2] = 0;
  data->day = atoi(ts);
  
  ts[0] = data->gpsdate[2];
  ts[1] = data->gpsdate[3];
  ts[2] = 0;
  data->month = atoi(ts);
  
  ts[0] = data->gpsdate[4];
  ts[1] = data->gpsdate[5];
  ts[2] = 0;
  data->year = atoi(ts);

  /* magnetic variation*/
  while(packet[i] != ',')
  {
    i++;
    gpindex++;
  }   

  i++;
  gpindex=0;

  /* reached here, nice! */

  /* Ivalid data if fix is 0 */
  if (data->fix_quality == '0')
    return 1;

  return 0;
}