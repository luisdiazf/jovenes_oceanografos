/*
* Test de lectura del módulo Grove-GPS
*
*/

#include "nmea.h"

nmea_data_t ndt;

 
unsigned char buffer[80];                   // buffer array for data receive over serial port
int count=0;                                // counter for buffer array
 
void setup()
{
    // the Serial baud rate
    Serial.begin(9600);                     // the Serial port of Arduino baud rate.
    SerialUSB.begin(115200);
    ndt = parse_nmea_init_data();
}
 
void loop()
{
    if (Serial.available())                     // if date is coming from software serial port ==> data is coming from Serial shield
    {
        int n;
      n = Serial.readBytesUntil('\n', buffer, 80);
      SerialUSB.println(n);
      
      
        SerialUSB.write(buffer,80);                 // if no data transmission ends, write buffer to hardware serial port

        count = 0;                                  // set counter of while loop to zero
 
 
    }
    if (SerialUSB.available())                 // if data is available on hardware serial port ==> data is coming from PC or notebook
    Serial.write(Serial.read());        // write it to the Serial shield
}
 
void clearBufferArray()                     // function to clear buffer array
{
    for (int i=0; i<count;i++)
    { buffer[i]=NULL;}                      // clear all index of array with command NULL
}
