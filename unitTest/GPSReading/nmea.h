#include <inttypes.h>

#define TIMEZONE_DEVIATION 1

struct nmea_data_struct {
  char gpstime[11];
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
  char gpsdate[11];
  uint8_t day;
  uint8_t month;
  uint8_t year;
  char latitude[16];
  char longitude[16];
  char ns[2];
  char ew[2];
  char fix_quality;
  char num_sats;
  char altitude[8];
};

typedef struct nmea_data_struct* nmea_data_t;

/* returns pointer to allocated memory for data structure */
nmea_data_t parse_nmea_init_data(void);

/* converts between NMEA and decimal degrees */
void parse_nmea_convert_degrees(char* coord);

/* parses nmea GGA string filling fields of data structure */
int parse_nmea_gga(nmea_data_t data, char* packet);

/* parses nmea RMC string filling fields of data structure */
int parse_nmea_rmc(nmea_data_t data, char* packet);


/* Example 
 *  
 *  
  
  int n;
  n = Serial.readBytesUntil('\n', nmeatemp, 80);
  nmeatemp[n] = 0;
  if (nmeatemp[3] == 'G' && nmeatemp[4] == 'G' && nmeatemp[5] == 'A') { 
    parse_nmea_gga(nmea_data, nmeatemp);
  }
  else if (nmeatemp[3] == 'R' && nmeatemp[4] == 'M' && nmeatemp[5] == 'C') { 
    parse_nmea_rmc(nmea_data, nmeatemp);
  }
    //SerialUSB.println(nmeatemp);

    SerialUSB.print(nmea_data->latitude);
    SerialUSB.print(" ");
    SerialUSB.print(nmea_data->ns);
    SerialUSB.print(", ");
    SerialUSB.print(nmea_data->longitude);
    SerialUSB.print(" ");
    SerialUSB.print(nmea_data->ew);
    SerialUSB.print(", ");
    SerialUSB.print(nmea_data->altitude);
    SerialUSB.print(", ");
    SerialUSB.print(nmea_data->hour);
    SerialUSB.print(":");
    SerialUSB.print(nmea_data->minute);
    SerialUSB.print(":");
    SerialUSB.print(nmea_data->second);
    SerialUSB.print(", ");
    SerialUSB.print(nmea_data->day);
    SerialUSB.print("/");
    SerialUSB.print(nmea_data->month);
    SerialUSB.print("/");
    SerialUSB.print(nmea_data->year);
    SerialUSB.print(", ");
    SerialUSB.println(nmea_data->num_sats);
    
 */