/*
 * Test de lectura en entradas "switched"
 * 
 * fabLAB Asturias 2016
 * David Pello
 * Luis Diaz
 */

#include <OneWire.h>
#include <DallasTemperature.h>


#define Pin 2 //Se declara el pin donde se conectará la DATA

OneWire ourWire(Pin); //Se establece el pin declarado como bus para la comunicación OneWire

DallasTemperature tempSensor(&ourWire); //Se instancia la librería DallasTemperature

int wait;

void setup() {
  delay(1000);
  SerialUSB.begin(9600);
  SerialUSB.println("Inicializando sensores...");
  tempSensor.begin(); //Se inician los sensores
  SerialUSB.println("Sensores inicializados");
    
}

void loop() {

  digitalWrite(VCC_SW, HIGH); //se enciende la alimentación en la columna de sensores "switched"
  
  
  delay(1); //delay para asegurar que el sensor está alimentado antes de inicializarlo
  tempSensor.begin(); //Se inician los sensores otra vez porque acaban de iniciar
  tempSensor.requestTemperatures(); //Prepara el sensor para la lectura
  SerialUSB.print(tempSensor.getTempCByIndex(0)); //Se lee e imprime la temperatura en grados Celsius
  SerialUSB.println(" grados Centigrados");
  SerialUSB.print(tempSensor.getTempFByIndex(0)); //Se lee e imprime la temperatura en grados Fahrenheit
  SerialUSB.println(" grados Fahrenheit");
  
  
  digitalWrite(VCC_SW, LOW); //se apaga la alimentación en la columna de sensores "switched"

  delay(5000); //Se provoca un lapso de 5 segundos antes de la próxima lectura

}
