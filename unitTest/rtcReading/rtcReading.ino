/*
 * Este programa es una prueba del funcionamiento del RTC interno
 * Puede configurarse una hora y fecha, al conectar por serie y enviar cualquier caracter, pone esa hora en el reloj
 * Si la placa pierde alimentación, perderá también la configuración de la hora
 * 
 */


#include <RTCZero.h>

/* Change these values to set the current initial time */
const uint8_t seconds = 0;
const uint8_t minutes = 22;
const uint8_t hours = 15;

/* Change these values to set the current initial date */
const uint8_t day = 1;
const uint8_t month = 2;
const uint8_t year = 16;

RTCZero rtc; 

void setup() {

  rtc.begin();

}

void loop() {

  SerialUSB.println("waiting for input");

  if(SerialUSB.available())
    {
      // Set the time
      rtc.setHours(hours);
      rtc.setMinutes(minutes);
      rtc.setSeconds(seconds);

      // Set the date
      rtc.setDay(day);
      rtc.setMonth(month);
      rtc.setYear(year);
    }

    SerialUSB.print(rtc.getHours());
    SerialUSB.print("\t");
    SerialUSB.print(rtc.getMinutes());
    SerialUSB.print("\t");
    SerialUSB.print(rtc.getSeconds());
    SerialUSB.print("\t");
    SerialUSB.print(rtc.getDay());
    SerialUSB.print("\t");
    SerialUSB.print(rtc.getMonth());
    SerialUSB.print("\t");
    SerialUSB.print(rtc.getYear());
    SerialUSB.println();

}
