
/*
* Autonomo board program for "Jovenes Oceanografos" project
* http://autonomo.sodaq.net/
* 
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this code.  If not, see <http://www.gnu.org/licenses/>.
* 
* fabLAB Asturias 2016
* David Pello
* Luis Diaz
*/
#include <inttypes.h>

#define TEMPPIN 2
#define FLUOPIN A0
#define DEVICE_ID 2
#define SLEEP_HR 0
#define SLEEP_MIN 20
#define SLEEP_SEC 0
#define MAX_LINE_SIZE 128

//#define USBMONITOR

#define APN "gprs-service.com"
#define APN_USERNAME ""
#define APN_PASSWORD ""



