/*
* Autonomo board program for "Jovenes Oceanografos" project
* http://autonomo.sodaq.net/
* http://www.oceanoabierto.es
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this code.  If not, see <http://www.gnu.org/licenses/>.
*
*IMPORTANTE: Usar tarjeta SIM sin PIN
*
* fabLAB Asturias 2016
* David Pello
* Luis Diaz
*/

#include "JovOceanFirm.h"

#include <OneWire.h>
#include <DallasTemperature.h>
#include <RTCZero.h>
#include <TinyGPS++.h>
#include <SPI.h>
#include <SD.h>
#include <GPRSbee.h>
#include <math.h>
#include <inttypes.h>
#include <avr/dtostrf.h>

//sensores
OneWire ourWire(TEMPPIN); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature tempSensor(&ourWire); //Se instancia la librería DallasTemperature

char* serverURL = "http://data.oceanoabierto.es:8080";

//GPS
TinyGPSPlus gps;

//Sensores
float temp;
int fluo;
float vBat;

char dataStr[MAX_LINE_SIZE];

RTCZero rtc;

//Logueo
File dataFile;
File notSentFile;
char filename[] = "LOGGER00.TXT";
char notSentFilename[] = "NOTSENT.TXT"; //OJO, hardcoded en función checkNotSent()
char loggerFilename[] = "LOGGER.TXT";
int lastSentData = 0;
int id = 0;


//##################################################  SETUP  #######################################################################
void setup() {
#ifdef USBMONITOR
  SerialUSB.begin(115200);
#endif
  Serial.begin(9600);
  Serial1.begin(57600);

  delay(6000);
  sensorsOn();
  float initTemp = readTemp();

  rtc.begin();

  //Inicializar SD
  while (!SD.begin(CS_SD))
  {
#ifdef USBMONITOR
    SerialUSB.println("Card failed, or not present");
    logToSd("Card failed, or not present");
#endif
    delay(3000);
  }

  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i / 10 + '0';
    filename[7] = i % 10 + '0';
    if (! SD.exists(filename)) {
#ifdef USBMONITOR
      SerialUSB.print("File name: ");
      SerialUSB.println(filename);
#endif
      break;  // leave the loop!
    }
  }

  //Esperar a tener fix del gps
  while (gps.sentencesWithFix() < 10)
  {
    smartDelay(1000);
#ifdef USBMONITOR
    SerialUSB.println("Not fixed yet at start");
    logToSd("Not fixed yet at start");
#endif
    blink(2, 200);
  }
#ifdef USBMONITOR
  SerialUSB.println("houston we have a fix");
#endif

  //Switch on the VCC for the Bee socket
  digitalWrite(BEE_VCC, HIGH);

  //Power switching is still being resolved
  gprsbee.init(Serial1, BEECTS, BEEDTR);
#ifdef USBMONITOR
  gprsbee.setDiag(SerialUSB);
#endif

  //poner en hora el rtc
  setRTC();
}

//##################################################  LOOP  ########################################################################

void loop() {
  digitalWrite(BEE_VCC, HIGH);
  smartDelay(500);

  //encender sensores
  sensorsOn();

  gprsbee.setPowerSwitchedOnOff(true);


  //esperar hasta que el GPS tenga posición
  while (!gps.location.isUpdated() || !gps.location.isValid() || !gps.time.isUpdated() || !gps.date.isValid() || !gps.date.isUpdated())
  {
    smartDelay(1000);
#ifdef USBMONITOR
    SerialUSB.println("Not fixed yet");
    logToSd("Esperando datos válidos de GPS");
#endif
    blink(2, 200);
  }

  //leer sensores
#ifdef USBMONITOR
  SerialUSB.println("Leyendo sensores");
  logToSd("Leyendo sensores");
#endif

  temp = readTemp();
  fluo = readFluo();
  vBat = readBatt();

  fillDataStr();
#ifdef USBMONITOR
  SerialUSB.println("dataStr completo");
  SerialUSB.print("String a enviar: ");
  SerialUSB.println(dataStr);

  logToSd("dataStr completo");
  logToSd("String a enviar: ");
  logToSd(dataStr);
#endif
  //comprobar que no hay datos sin enviar
  checkNotSent(notSentFilename);

  //envío de datos, si tiene éxito, registrar el último dato enviado
  bool sendOk = false;
  sendOk = sendData(dataStr);

  //TODO: reintentar n veces

  if (!sendOk) {
    //añadir el último data a notSentFile
    #ifdef USBMONITOR
        SerialUSB.println("Fallo el envio");
        logToSd("Fallo el envio");
    #endif
    writeToSd(notSentFilename, dataStr);
  }
  //loguear en tarjeta
  writeToSd(filename, dataStr);

  //apagar sensores
  sensorsOff();

  //Si todo OK, dormir
  goToSleep(SLEEP_HR, SLEEP_MIN, SLEEP_SEC);
} //fin loop()

void checkNotSent(char* fileNotSent) {
  unsigned long pos = 0;
  long lineStart = 0;
  long nextLineStart = 0;

#ifdef USBMONITOR
  SerialUSB.println(SD.exists("notsent.txt"));
#endif

  if (SD.exists("notsent.txt")) {

    notSentFile = SD.open(fileNotSent, FILE_READ);

    if (notSentFile.size() != 0)
    {
#ifdef USBMONITOR
      SerialUSB.println("hay datos sin enviar");
      logToSd("hay datos sin enviar");
#endif

      while (notSentFile.peek() != -1) { //mientras no llegue al final del archivo

#ifdef USBMONITOR
        SerialUSB.println("no es el final del archivo");
#endif

        int count = 0;
        char line[MAX_LINE_SIZE];

        while (notSentFile.peek() != '\n' && notSentFile.peek() != 'X')
        {
          line[count] = notSentFile.read();
          count++;
        }
        //notSentFile.read(); //saltar el \n, TODO hacer con pos
        pos = notSentFile.position();
        lineStart = pos - count;

#ifdef USBMONITOR
        SerialUSB.print("lineStart: ");
        SerialUSB.println(lineStart);
#endif

        pos++;
        nextLineStart = pos;
#ifdef USBMONITOR
        SerialUSB.print("nextLineStart: ");
        SerialUSB.println(nextLineStart);
        SerialUSB.print("Enviando datos antiguos sin enviar: ");
#endif

        //String oldData(line);
#ifdef USBMONITOR
        SerialUSB.println(line);
#endif

        bool sendOldOk;
        sendOldOk = sendData(line);

        //llenar de X esa línea
#ifdef USBMONITOR
        SerialUSB.println(sendOldOk);
#endif

        if (sendOldOk)
        {
          notSentFile.seek(lineStart);
          while (notSentFile.peek() != '\n')
          {
            notSentFile.print('X');
            pos = notSentFile.position();
            pos++;
            notSentFile.seek(pos);
          }
        }
        //volver al principio de la siguiente línea
        notSentFile.seek(nextLineStart);
      }
    }
    notSentFile.close();
    SD.remove(notSentFilename);
  }
}

bool sendData(char data[MAX_LINE_SIZE]) {
  //char data[MAX_LINE_SIZE];

  //datos.toCharArray(data, datos.length());
  char buffer[512];
  memset(buffer, '\0', sizeof(buffer));

#ifdef USBMONITOR
  SerialUSB.print("sendData: ");
  SerialUSB.println(data);
  logToSd("enviando datos");
#endif

  bool retval = gprsbee.doHTTPPOSTWithReply(APN, APN_USERNAME, APN_PASSWORD,
                serverURL, data, strlen(data), buffer, sizeof(buffer));

#ifdef USBMONITOR
  SerialUSB.print("Resultado peticion: ");
  SerialUSB.println(retval);

  //si tengo ok, devuelvo true
  SerialUSB.println("POST result: ");

  //Lets be safe in case result length > 247
  for (int i = 0; i < strlen(buffer); i++) {
    SerialUSB.print(buffer[i]);
  }
#endif

  //if(buffer[5] = 'W') SerialUSB.println("WIN");
  if (buffer[5] = 'W') return true;
  //if(buffer[5] = 'F') SerialUSB.println("FAIL");
  if (buffer[5] = 'F') return false;

  /*
  Las respuestas del server:
  'EPIC WIN!'
  'EPIC FAIL!'
  */

  return false;

}

void writeToSd(char* nombreArchivo, char* datos) {
  /*
   * A guardar
   * dev_id, id , tempValue, fluoValue, lat, long, hour, minute, second, day, month, year, satelites, velocidad, nivel de bateria,
   */
  File archivo = SD.open(nombreArchivo, FILE_WRITE);

#ifdef USBMONITOR
  SerialUSB.print("Writing to SD:   ");
  SerialUSB.println(datos);
#endif

  if (archivo) {
    archivo.print(datos);
    archivo.print('\n');
  }
  archivo.close();


}

void logToSd(char* datos) {
  /*
   * A guardar
   * dev_id, id , tempValue, fluoValue, lat, long, hour, minute, second, day, month, year, satelites, velocidad, nivel de bateria,
   */
  File archivo = SD.open(loggerFilename, FILE_WRITE);

  if (archivo) {

    archivo.print(datos);
    archivo.print('\n');
  }
  archivo.close();


}

void printData(String datos) {
  SerialUSB.print("Mostrando datos: ");
  SerialUSB.println(datos);
}

static void goToSleep(int hours, int minutes, int seconds) {

  digitalWrite(BEE_VCC, LOW);

  int nowSecond = rtc.getSeconds();
  int nowMinute = rtc.getMinutes();
  int nowHour = rtc.getHours();

  if ((nowSecond + seconds) >= 60)
  {
    minutes++;
    seconds -= 60;
  }

  if ((nowMinute + minutes) >= 60)
  {
    hours++;
    minutes -= 60;
  }

  if ((nowHour + hours) > 23){
    nowHour = 0;
    hours = 0;
  }


  rtc.setAlarmHours(nowHour + hours);
  rtc.setAlarmMinutes(nowMinute + minutes);
  rtc.setAlarmSeconds(nowSecond + seconds);

#ifdef USBMONITOR
  logToSd("alarma puesta en hora");
  char hora[10];
  char horadealarma[10];
  sprintf(hora, "%i:%i:%i", nowHour, nowMinute, nowSecond);

  sprintf(horadealarma, "%i:%i:%i", hours, minutes, seconds);
  logToSd(hora);
  logToSd(horadealarma);
  SerialUSB.print("Son las : ");
  SerialUSB.print(rtc.getHours());
  SerialUSB.print(":");
  SerialUSB.print(rtc.getMinutes());
  SerialUSB.print(":");
  SerialUSB.print(rtc.getSeconds());
  SerialUSB.println();

  SerialUSB.print("Alarma puesta para las : ");
  SerialUSB.print(rtc.getHours() + hours);
  SerialUSB.print(":");
  SerialUSB.print(rtc.getMinutes() + minutes);
  SerialUSB.print(":");
  SerialUSB.print(rtc.getSeconds() + seconds);
  SerialUSB.println();
#endif

  rtc.enableAlarm(RTCZero::MATCH_HHMMSS);
  SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

  // Disable USB
#ifdef USBMONITOR
  USB->DEVICE.CTRLA.reg &= ~USB_CTRLA_ENABLE;
#endif
  digitalWrite(LED_BUILTIN, LOW);
  //Enter sleep mode
  __WFI();
  digitalWrite(LED_BUILTIN, HIGH);
  // ...Sleep

  // Enable USB
#ifdef USBMONITOR
  logToSd("sali de dormir");
  USB->DEVICE.CTRLA.reg |= USB_CTRLA_ENABLE;
#endif
}

void blink(int reps, int interval) {
  for (int i = 0; i <= reps; i++)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(interval);
    digitalWrite(LED_BUILTIN, LOW);
    delay(interval);
  }

  delay(1000);
}

static void smartDelay(unsigned long ms) {
  unsigned long start = millis();
  do
  {
    while (Serial.available())
      gps.encode(Serial.read());
  } while (millis() - start < ms);
}

void setRTC() {
  rtc.setHours(gps.time.hour());
  rtc.setMinutes(gps.time.minute());
  rtc.setSeconds(gps.time.second());
  rtc.setDay(gps.date.day());
  rtc.setMonth(gps.date.month());
  rtc.setYear(gps.date.year());
}

void printGPSData() {
  SerialUSB.print("GPS hour :");
  SerialUSB.print(gps.time.hour());

  SerialUSB.print(" GPS minute:");
  SerialUSB.print(gps.time.minute());

  SerialUSB.print(" GPS second:");
  SerialUSB.print(gps.time.second());

  SerialUSB.print(" GPS day:");
  SerialUSB.print(gps.date.day());

  SerialUSB.print(" GPS month:");
  SerialUSB.print(gps.date.month());

  SerialUSB.print(" GPS year:");
  SerialUSB.print(gps.date.year());

  SerialUSB.print(" Latitude:");
  SerialUSB.print(gps.location.lat(), 4);
  SerialUSB.print(" Longitude:");
  SerialUSB.print(gps.location.lng());
  SerialUSB.print(" num_sats:");
  SerialUSB.print(gps.satellites.value());
  SerialUSB.print(" sentenceswithfix");
  SerialUSB.print(gps.sentencesWithFix());
  SerialUSB.println();

}

//Sensores
float readTemp(void) {
  tempSensor.begin();
  tempSensor.requestTemperatures();
  return tempSensor.getTempCByIndex(0);
}

int readFluo(void) {
  long randNumber = random(100,1000);
  return randNumber;
}

float readBatt() {
  return fscale(0, 1023, 0, 4.92, analogRead(BAT_VOLT), 0);
}

void sensorsOn (void) {
  digitalWrite(VCC_SW, HIGH);
  delay(1);

}

void sensorsOff (void) {
  digitalWrite(VCC_SW, LOW);
  delay(1);

}

void fillDataStr() {

  memset(dataStr, 0, sizeof dataStr);

  //1,0,float,-1,double,double,9,46,54,21,3,2016,5,float,double

  char strTemp[6];
  dtostrf (temp, 2, 2, strTemp);

  char strLat[10];
  char strLng[10];
  dtostrf (gps.location.lat(), 2, 6, strLat);
  dtostrf (gps.location.lng(), 2, 6, strLng);

  char strvBat[5];
  dtostrf (vBat, 1, 2, strvBat);

  char strSpeed[6];
  dtostrf (gps.speed.kmph(), 2, 2, strSpeed);

  sprintf(dataStr, "%i,%i,%s,%i,%s,%s,%u,%u,%u,%u,%u,%" PRIu16 ",%" PRIu32 ",%s,%s",
          DEVICE_ID,
          id,
          strTemp,
          fluo,
          strLat,
          strLng,
          gps.time.hour(),    //uint_8t
          gps.time.minute(),
          gps.time.second(),
          gps.date.day(),
          gps.date.month(),
          gps.date.year(), //uint_16t
          gps.satellites.value(), //uint_32t
          strvBat,
          strSpeed
         );
}

float fscale( float originalMin, float originalMax, float newBegin, float newEnd, float inputValue, float curve) {

  float OriginalRange = 0;
  float NewRange = 0;
  float zeroRefCurVal = 0;
  float normalizedCurVal = 0;
  float rangedValue = 0;
  boolean invFlag = 0;


  // condition curve parameter
  // limit range

  if (curve > 10) curve = 10;
  if (curve < -10) curve = -10;

  curve = (curve * -.1) ; // - invert and scale - this seems more intuitive - postive numbers give more weight to high end on output
  curve = pow(10, curve); // convert linear scale into lograthimic exponent for other pow function

  /*
   Serial.println(curve * 100, DEC);   // multply by 100 to preserve resolution
   Serial.println();
   */

  // Check for out of range inputValues
  if (inputValue < originalMin) {
    inputValue = originalMin;
  }
  if (inputValue > originalMax) {
    inputValue = originalMax;
  }

  // Zero Refference the values
  OriginalRange = originalMax - originalMin;

  if (newEnd > newBegin) {
    NewRange = newEnd - newBegin;
  }
  else
  {
    NewRange = newBegin - newEnd;
    invFlag = 1;
  }

  zeroRefCurVal = inputValue - originalMin;
  normalizedCurVal  =  zeroRefCurVal / OriginalRange;   // normalize to 0 - 1 float

  // Check for originalMin > originalMax  - the math for all other cases i.e. negative numbers seems to work out fine
  if (originalMin > originalMax ) {
    return 0;
  }

  if (invFlag == 0) {
    rangedValue =  (pow(normalizedCurVal, curve) * NewRange) + newBegin;

  }
  else     // invert the ranges
  {
    rangedValue =  newBegin - (pow(normalizedCurVal, curve) * NewRange);
  }

  return rangedValue;
}
